package com.kodgemisi.database.dbexamples;

import com.kodgemisi.database.dbexamples.domain.Event;
import com.kodgemisi.database.dbexamples.domain.EventCategory;
import com.kodgemisi.database.dbexamples.domain.Venue;
import com.kodgemisi.database.dbexamples.repository.EventCategoryRepository;
import com.kodgemisi.database.dbexamples.repository.EventRepository;
import com.kodgemisi.database.dbexamples.repository.VenueRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }


    @Bean
    public CommandLineRunner demoExpenseType(VenueRepository venueRepository, EventCategoryRepository eventCategoryRepository, EventRepository eventRepository) {
        return (args) -> {
            Venue v1 = venueRepository.save(new Venue("kofteci osman"));
            Venue v2 = venueRepository.save(new Venue("yildiz kafe"));

            Event ev1 = eventRepository.save(new Event("Serdar Ortac"));
            Event ev2 = eventRepository.save(new Event("Teoman"));
            Event ev3 = eventRepository.save(new Event("Gulsen"));

            EventCategory e1 = eventCategoryRepository.save(new EventCategory("rock"));
            EventCategory e2 = eventCategoryRepository.save(new EventCategory("pop"));
            EventCategory e3 = eventCategoryRepository.save(new EventCategory("blues"));


            Set<Event> v1Events = new HashSet<Event>();

            v1Events.add(ev2);

            v1.setEvents(v1Events);

            venueRepository.save(v1);


            Set<Event> v2Events = new HashSet<Event>();

            v2Events.add(ev1);
            v2Events.add(ev3);

            v2.setEvents(v2Events);

            venueRepository.save(v2);


            Set<EventCategory> ev1Categories = new HashSet<EventCategory>();

            ev1Categories.add(e1);
            ev1Categories.add(e2);

            ev1.setEventCategories(ev1Categories);

            eventRepository.save(ev1);


            Set<EventCategory> ev2Categories = new HashSet<EventCategory>();

            ev2Categories.add(e2);
            ev2Categories.add(e3);

            ev2.setEventCategories(ev2Categories);

            eventRepository.save(ev2);

            Set<EventCategory> ev3Categories = new HashSet<EventCategory>();

            ev3Categories.add(e2);

            ev3.setEventCategories(ev3Categories);

            eventRepository.save(ev3);
        };
    }


}