package com.kodgemisi.database.dbexamples.controller;

import com.kodgemisi.database.dbexamples.service.VenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created on July, 2016
 *
 * @author xaph
 */

@Controller
public class DashboardController {


    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    VenueService venueService;

    @RequestMapping("/")
    String home(Model model) {
        return "index";
    }


    //example sql injection
    //localhost:8080/jdbc?name=yildiz'; insert into venue(id, name) values (3, 'karsiyaka');--
    @RequestMapping("/jdbc")
    String jdbcExample(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "secure", required = false) String isSecure, Model model) {

        //localhost:8080/jdbc?secure=true&name=yildiz'; insert into venue(id, name) values (3, 'karsiyaka');--
        if (isSecure != null) {
            model.addAttribute("venues", jdbcTemplate.queryForList("SELECT * FROM VENUE where name like ?", name));
        } else if (name == null) {
            model.addAttribute("venues", jdbcTemplate.queryForList("SELECT * FROM VENUE"));
        } else {
            model.addAttribute("venues", jdbcTemplate.queryForList("SELECT * FROM VENUE where name like '%" + name + "%'"));
        }

        return "jdbc";
    }

    @RequestMapping("/jdbc_complex")
    String jdbcComplexExample(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "secure", required = false) String isSecure, Model model) {


        //localhost:8080/jdbc?secure=true&name=yildiz'; insert into venue(id, name) values (3, 'karsiyaka');--
        if (isSecure != null) {
            model.addAttribute("venues", jdbcTemplate.queryForList(
                    "SELECT v.id id, v.name venue_name, ec.name cat_name, ev.name event_name FROM VENUE v\n"
                            + "LEFT JOIN VENUE_EVENTS ve ON v.id=ve.venue_id\n"
                            + "LEFT JOIN EVENT ev on ev.id = ve.event_id\n"
                            + "LEFT JOIN EVENT_EVENT_CATEGORIES ece on ev.id = ece.event_id\n"
                            + "LEFT JOIN EVENT_CATEGORY ec on ec.id = ece.event_category_id where v.name = ?;", name
            ));
        } else if (name == null) {
            model.addAttribute("venues", jdbcTemplate.queryForList(
                    "SELECT v.id id, v.name venue_name, ec.name cat_name, ev.name event_name FROM VENUE v\n"
                            + "LEFT JOIN VENUE_EVENTS ve ON v.id=ve.venue_id\n"
                            + "LEFT JOIN EVENT ev on ev.id = ve.event_id\n"
                            + "LEFT JOIN EVENT_EVENT_CATEGORIES ece on ev.id = ece.event_id\n"
                            + "LEFT JOIN EVENT_CATEGORY ec on ec.id = ece.event_category_id;"
            ));

        } else {

            model.addAttribute("venues", jdbcTemplate.queryForList(
                    "SELECT v.id id, v.name venue_name, ec.name cat_name, ev.name event_name FROM VENUE v\n"
                            + "LEFT JOIN VENUE_EVENTS ve ON v.id=ve.venue_id\n"
                            + "LEFT JOIN EVENT ev on ev.id = ve.event_id\n"
                            + "LEFT JOIN EVENT_EVENT_CATEGORIES ece on ev.id = ece.event_id\n"
                            + "LEFT JOIN EVENT_CATEGORY ec on ec.id = ece.event_category_id where v.name='" + name + "';"
            ));
        }

        return "jdbc_complex";
    }

    @RequestMapping("/criteriaList")
    String getVenuesByCriteria(@RequestParam(value = "name", required = false) String name, Model model) {

        if (StringUtils.isEmpty(name)) {
            model.addAttribute("venues", venueService.getVenues());
        } else {
            model.addAttribute("venues", venueService.getVenuesByName(name));
        }

        return "criteria_list";
    }

}
