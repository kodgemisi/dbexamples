package com.kodgemisi.database.dbexamples.service;

import com.kodgemisi.database.dbexamples.domain.Venue;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

/**
 * Created on August, 2016
 *
 * @author xaph
 */
@Service
@Transactional
public class VenueService {
	@Autowired
	private SessionFactory sessionFactory;

	public HashSet<Venue> getVenues() {
		return new HashSet<Venue>(sessionFactory.getCurrentSession().createCriteria(Venue.class)
				.setFetchMode("events", FetchMode.JOIN)
				.setFetchMode("events.eventCategories", FetchMode.JOIN)
				.list());
	}

	public HashSet<Venue> getVenuesByName(String name) {
		return new HashSet<Venue>(sessionFactory.getCurrentSession().createCriteria(Venue.class)
				.setFetchMode("events", FetchMode.JOIN)
				.setFetchMode("events.eventCategories", FetchMode.JOIN)
				.add(Restrictions.like("name", name))
				.list());
	}

}
