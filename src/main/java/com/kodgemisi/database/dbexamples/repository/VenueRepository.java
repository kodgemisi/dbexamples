package com.kodgemisi.database.dbexamples.repository;

import com.kodgemisi.database.dbexamples.domain.Venue;
import org.springframework.data.repository.CrudRepository;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
public interface VenueRepository extends CrudRepository<Venue, Long> {


}
