package com.kodgemisi.database.dbexamples.repository;

import com.kodgemisi.database.dbexamples.domain.EventCategory;
import org.springframework.data.repository.CrudRepository;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
public interface EventCategoryRepository extends CrudRepository<EventCategory, Long> {
}
