package com.kodgemisi.database.dbexamples.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
@Entity
public class Event {
	public Event() {

	}

	public Event(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@NotNull
	private String name;

	@ManyToMany(mappedBy="events")
	private Set<Venue> venues;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
			joinColumns=@JoinColumn(name = "event_id"),
			inverseJoinColumns = @JoinColumn(name="event_category_id")
	)
	private Set<EventCategory> eventCategories = new HashSet<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Venue> getVenues() {
		return venues;
	}

	public void setVenues(Set<Venue> venues) {
		this.venues = venues;
	}

	public Set<EventCategory> getEventCategories() {
		return eventCategories;
	}

	public void setEventCategories(Set<EventCategory> eventCategories) {
		this.eventCategories = eventCategories;
	}
}
