package com.kodgemisi.database.dbexamples.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
@Entity
public class EventCategory {
	public EventCategory() {

	}

	public EventCategory(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@NotNull
	private String name;

	@ManyToMany(mappedBy="eventCategories")
	private Set<Event> events = new HashSet<>();;



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}
}
