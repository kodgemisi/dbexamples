package com.kodgemisi.database.dbexamples.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
@Entity
public class Address {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@NotNull
	private String fullAddress;

	private Long lat;

	private Long lon;

	@ManyToOne(fetch = FetchType.LAZY)
	private Venue venue;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public Long getLat() {
		return lat;
	}

	public void setLat(Long lat) {
		this.lat = lat;
	}

	public Long getLon() {
		return lon;
	}

	public void setLon(Long lon) {
		this.lon = lon;
	}

	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
}
