package com.kodgemisi.database.dbexamples.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on July, 2016
 *
 * @author xaph
 */
@Entity
public class Venue {
	public Venue(){

	}

	public Venue(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "venue")
	private Set<Address> addresses = new HashSet<>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
			joinColumns=@JoinColumn(name = "venue_id"),
			inverseJoinColumns = @JoinColumn(name="event_id")
	)
	private Set<Event> events = new HashSet<>();;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Venue venue = (Venue) o;
		return id == venue.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
